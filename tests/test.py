import unittest
from demo_pkg.core import hello_pipe


class Test(unittest.TestCase):
    # dummy test
    def test_core_success(self):
        assert 'Pipes are awesome!' == hello_pipe()
