import os
from setuptools import setup


NAME = 'demo_pkg'
VERSION = None
BASE_DIR = os.path.abspath(os.path.dirname(__file__))


# Load the package's __version__.py module as a dictionary.
package_metainfo = {}
if not VERSION:
    with open(os.path.join(BASE_DIR, NAME, '__version__.py')) as f:
        exec(f.read(), package_metainfo)
else:
    package_metainfo['__version__'] = VERSION


# Packaging and distributing projects
# https://packaging.python.org/guides/distributing-packages-using-setuptools/
setup(
    name=NAME,
    version=package_metainfo['__version__'],
    url='https://example.com',
    author='Atlassian',
    author_email='bitbucketci-team@atlassian.com',
    description='Test package',
    license='MIT',
)
