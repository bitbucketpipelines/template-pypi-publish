
def hello_pipe():
    return 'Pipes are awesome!'


def main():
    print(hello_pipe())


if __name__ == '__main__':
    main()
